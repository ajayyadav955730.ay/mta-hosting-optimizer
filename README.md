# MTA Hosting Optimizer

## Overview

The MTA Hosting Optimizer is a service designed to optimize the hosting of mail transfer agents (MTAs) on physical servers, aiming for service that uncovers the inefficient servers hosting only few active MTAs.


## Features

- **HTTP/REST Endpoint:** Provides an endpoint to retrieve hostnames with a configurable number of active IP addresses (X).
- **Configurability:** X is configurable through an environment variable, with a default value of 1.
- **Mock IP Configuration:** Utilizes a mock service with sample data for IP configuration.
- **Code Coverage:** Code coverage exceeds 90%, ensuring robust testing.
- **Integration with GitHub Actions:** Integrates the test and build phases into GitHub Actions.

## Getting Started

### Prerequisites

- Go installed on your machine.

### Installation

1. Clone the GitLab repository:

    ```bash
    git clone https://gitlab.com/yourusername/mta-hosting-optimizer.git
    ```

2. Navigate to the project directory:

    ```bash
    cd mta-hosting-optimizer
    ```

3. Build the project:

    ```bash
    go build
    ```

## Usage

Describe how users can use your service, including details about interacting with the HTTP/REST endpoint and any other relevant information.

## Configuration

- **X Configuration:**
  - The number of active IP addresses (X) is configurable through the `X_ACTIVE` environment variable. The default value is 1.

## Testing

Explain how to run unit and integration tests for the project.

## Code Coverage

Ensure that the code coverage exceeds 90%. You can check the coverage by running:

```bash
go test -cover ./...