package host

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mta-hosting-optimizer/services"
)

type Host struct {
	svc services.Hoster
}

func New(h services.Hoster) *Host {
	return &Host{svc: h}
}

func (h *Host) GetHostnamesWithActiveIPs(ctx *gin.Context) {
	hostnames := h.svc.GetHostnames(ctx)
	ctx.JSON(200, hostnames)
}
