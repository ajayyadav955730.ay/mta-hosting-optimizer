package host

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mta-hosting-optimizer/services"
)

func Test_GetHostnamesWithActiveIPs(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx, _ := gin.CreateTestContext(httptest.NewRecorder())

	mockHostSvc := services.NewMockHoster(ctrl)

	handler := New(mockHostSvc)

	tests := []struct {
		desc         string
		mockCall     interface{}
		request      *http.Request
		expectedResp interface{}
		expectedErr  error
	}{
		{
			desc:         "success",
			request:      httptest.NewRequest("GET", "/getHostnamesWithActiveIPs", http.NoBody),
			mockCall:     mockHostSvc.EXPECT().GetHostnames(ctx).Return([]string{"mta-prod-1"}),
			expectedResp: []string{"mta-prod-1"},
			expectedErr:  nil,
		},
	}

	for _, tc := range tests {
		ctx.Request = tc.request
		handler.GetHostnamesWithActiveIPs(ctx)
		assert.Equal(t, http.StatusOK, ctx.Writer.Status())
	}
}
