package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	hostHTTP "gitlab.com/mta-hosting-optimizer/http/host"
	hostSvc "gitlab.com/mta-hosting-optimizer/services/host"
	hostStore "gitlab.com/mta-hosting-optimizer/stores/host"
)

func main() {
	router, port := setupServer()
	err := router.Run(":" + port)
	if err != nil {
		log.Printf("Could not start the server, Err: %v", err)
	}
}

func setupServer() (*gin.Engine, string) {
	router := gin.Default()
	store := hostStore.NewMockIPConfigStore()
	svc := hostSvc.New(store)
	ipConfigService := hostHTTP.New(svc)

	router.GET("/getHostnamesWithActiveIPs", func(c *gin.Context) {
		ipConfigService.GetHostnamesWithActiveIPs(c)
	})

	// Run the server
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	return router, port
}
