package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mta-hosting-optimizer/constants"
)

// Test_Main tests the integration of the getHostnamesWithActiveIPs endpoint.
func Test_Main(t *testing.T) {
	t.Setenv(constants.XActive, "2")
	router, _ := setupServer()
	ts := httptest.NewServer(router)
	defer ts.Close()

	t.Run("it should return 200 ", func(t *testing.T) {
		resp, err := http.Get(fmt.Sprintf("%s/getHostnamesWithActiveIPs", ts.URL))

		if err != nil {
			t.Fatalf("Expected no error, got %v", err)
		}

		assert.Equal(t, 200, resp.StatusCode)
	})

}
