package models

type IPConfig struct {
	IP       string
	Hostname string
	Active   bool
}
