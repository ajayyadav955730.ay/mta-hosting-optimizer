package host

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/mta-hosting-optimizer/constants"
	"gitlab.com/mta-hosting-optimizer/stores"
)

type host struct {
	store stores.Hoster
}

func New(h stores.Hoster) *host {
	return &host{store: h}
}

// GetHostnames retrieves hostnames having less than or equals to X active IP addresses.
func (h *host) GetHostnames(c *gin.Context) []string {
	x, err := getThresholdX()
	if err != nil {
		log.Printf("Error while getting threshold X, Err: %v. Setting default value as %v", err, constants.DefaultXValue)
		x = constants.DefaultXValue
	}

	ipConfigs := h.store.GetIPConfigs()

	resultSet := make(map[string]int)
	for _, ipConfig := range ipConfigs {
		if ipConfig.Active {
			if count, ok := resultSet[ipConfig.Hostname]; ok {
				resultSet[ipConfig.Hostname] = count + 1
			} else {
				resultSet[ipConfig.Hostname] = 1
			}
		} else if _, ok := resultSet[ipConfig.Hostname]; !ok {
			resultSet[ipConfig.Hostname] = 0
		}
	}

	filteredHostnames := []string{}
	for hostname, count := range resultSet {
		if count <= x {
			filteredHostnames = append(filteredHostnames, hostname)
		}
	}

	return filteredHostnames
}

func getThresholdX() (int, error) {
	var x int
	var err error
	xStr := os.Getenv(constants.XActive)
	if xStr == "" {
		err = fmt.Errorf("X is not set")
	} else {
		x, err = strconv.Atoi(xStr)
		if err != nil {
			err = fmt.Errorf("Error converting X to integer: %v", err)
		}
	}

	return x, err
}
