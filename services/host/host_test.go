package host

import (
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/bmizerany/assert"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"gitlab.com/mta-hosting-optimizer/constants"
	"gitlab.com/mta-hosting-optimizer/models"
	"gitlab.com/mta-hosting-optimizer/stores"
)

func Test_GetHostnames(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	ctx, _ := gin.CreateTestContext(httptest.NewRecorder())

	mockHost := stores.NewMockHoster(ctrl)
	service := New(mockHost)

	tests := []struct {
		desc         string
		mockCall     interface{}
		expectedResp []string
		threshold    string
	}{
		{
			desc: "store layer error",
			mockCall: mockHost.EXPECT().GetIPConfigs().Return([]*models.IPConfig{
				{IP: "127.0.0.1", Hostname: "mta-prod-1", Active: true},
				{IP: "127.0.0.2", Hostname: "mta-prod-1", Active: false},
				{IP: "127.0.0.3", Hostname: "mta-prod-2", Active: true},
				{IP: "127.0.0.4", Hostname: "mta-prod-2", Active: true},
				{IP: "127.0.0.5", Hostname: "mta-prod-2", Active: false},
			}),
			threshold:    "",
			expectedResp: []string{"mta-prod-1"},
		},
		{
			desc: "store layer error",
			mockCall: mockHost.EXPECT().GetIPConfigs().Return([]*models.IPConfig{
				{IP: "127.0.0.2", Hostname: "mta-prod-1", Active: false},
				{IP: "127.0.0.1", Hostname: "mta-prod-1", Active: true},
				{IP: "127.0.0.2", Hostname: "mta-prod-1", Active: true},
			}),
			threshold:    "1",
			expectedResp: []string{},
		},
	}

	for _, tc := range tests {
		t.Setenv(constants.XActive, tc.threshold)
		resp := service.GetHostnames(ctx)
		assert.Equal(t, tc.expectedResp, resp)
	}
}

func TestGetThresholdX(t *testing.T) {

	tests := []struct {
		desc          string
		envValue      string
		expectedX     int
		expectedError error
	}{
		{
			desc:          "Valid X value",
			envValue:      "1",
			expectedX:     1,
			expectedError: nil,
		},
		{
			desc:          "X not set",
			envValue:      "",
			expectedX:     0,
			expectedError: fmt.Errorf("X is not set"),
		},
		{
			desc:          "Invalid X value",
			envValue:      "invalid",
			expectedX:     0,
			expectedError: fmt.Errorf("Error converting X to integer: strconv.Atoi: parsing \"invalid\": invalid syntax"),
		},
	}

	for _, tc := range tests {
		t.Setenv(constants.XActive, tc.envValue)
		x, err := getThresholdX()

		assert.Equal(t, tc.expectedX, x)
		assert.Equal(t, tc.expectedError, err)

	}
}
