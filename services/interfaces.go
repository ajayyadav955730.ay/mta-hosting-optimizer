package services

import (
	"github.com/gin-gonic/gin"
)

type Hoster interface {
	GetHostnames(c *gin.Context) []string
}
