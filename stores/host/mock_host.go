package host

import "gitlab.com/mta-hosting-optimizer/models"

type MockIPConfigStore struct{}

func NewMockIPConfigStore() *MockIPConfigStore {
	return &MockIPConfigStore{}
}

func (m *MockIPConfigStore) GetIPConfigs() []*models.IPConfig {
	return []*models.IPConfig{
		{IP: "127.0.0.1", Hostname: "mta-prod-1", Active: true},
		{IP: "127.0.0.2", Hostname: "mta-prod-1", Active: false},
		{IP: "127.0.0.3", Hostname: "mta-prod-2", Active: true},
		{IP: "127.0.0.4", Hostname: "mta-prod-2", Active: true},
		{IP: "127.0.0.5", Hostname: "mta-prod-2", Active: false},
		{IP: "127.0.0.6", Hostname: "mta-prod-3", Active: false},
		{IP: "127.0.0.6", Hostname: "mta-prod-4", Active: true},
		{IP: "127.0.0.6", Hostname: "mta-prod-4", Active: true},
	}
}
