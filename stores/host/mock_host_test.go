package host

import (
	"testing"

	"github.com/bmizerany/assert"
	"gitlab.com/mta-hosting-optimizer/models"
)

func Test_GetIPConfigs(t *testing.T) {
	mockStore := NewMockIPConfigStore()

	tests := []struct {
		desc         string
		expectedResp []*models.IPConfig
	}{
		{
			desc: "success",
			expectedResp: []*models.IPConfig{
				{IP: "127.0.0.1", Hostname: "mta-prod-1", Active: true},
				{IP: "127.0.0.2", Hostname: "mta-prod-1", Active: false},
				{IP: "127.0.0.3", Hostname: "mta-prod-2", Active: true},
				{IP: "127.0.0.4", Hostname: "mta-prod-2", Active: true},
				{IP: "127.0.0.5", Hostname: "mta-prod-2", Active: false},
				{IP: "127.0.0.6", Hostname: "mta-prod-3", Active: false},
				{IP: "127.0.0.6", Hostname: "mta-prod-4", Active: true},
				{IP: "127.0.0.6", Hostname: "mta-prod-4", Active: true},
			},
		},
	}

	for _, tc := range tests {
		resp := mockStore.GetIPConfigs()
		assert.Equal(t, tc.expectedResp, resp)
	}
}
