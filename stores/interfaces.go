package stores

import "gitlab.com/mta-hosting-optimizer/models"

type Hoster interface {
	GetIPConfigs() []*models.IPConfig
}
